# Mozblock

A blocky font made in a blocky world.

Made in the "Design open web fonts in Minecraft" at Mozilla Festival 2014.
Designed and built in 3 hours. Read more about the workshop at the [Type:Bits
website](https://typebits.gitlab.io).

![Font preview](https://gitlab.com/typebits/font-mozblock/-/jobs/artifacts/master/raw/Mozblock-Regular.png?job=build-font)
  
[See it in action](https://typebits.gitlab.io/font-mozblock/)

Download formats:

* [TrueType (.ttf)](https://gitlab.com/typebits/font-mozblock/-/jobs/artifacts/master/raw/Mozblock-Regular.ttf?job=build-font)
* [OpenType (.otf)](https://gitlab.com/typebits/font-mozblock/-/jobs/artifacts/master/raw/Mozblock-Regular.otf?job=build-font)
* [FontForge (.sfd)](https://gitlab.com/typebits/font-mozblock/-/jobs/artifacts/master/raw/Mozblock-Regular.sfd?job=build-font)

# Authors

* Jens Aronsson
* Johan Hermansson
* Jennifer Steele
* David Moulton
* Metod Blejec
* Louis Reed
* Adam Sofokleous
* Oliver Roick
* Camellia Xueyi

# License

This font is made available under the [Open Font License](https://scripts.sil.org/OFL).
